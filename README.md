# Memeify

Turn your boring normal text into meme text.

# Getting Started
1. Head over to the [Discord Developer Portal](https://discordapp.com/developers/applications) and create an application with a bot.
2. Clone this repository 
  ```
  git clone git@gitlab.com:brandonp2412/memeify.git
  ```
3. Copy the `.env.example` file and set the property `TOKEN` to your [bot token](https://github.com/Chikachi/DiscordIntegration/wiki/How-to-get-a-token-and-channel-ID-for-Discord).
```shell
cp .env.example .env
```

### Running Locally
Install [Python](https://www.python.org/downloads/) version 3.6 or higher.
```shell
pip install discord.py coloredlogs python-dotenv
python memeify.py
2019-04-22 14:13:56 PC_NAME __main__[12232] INFO Logged in as user user#1234
```

### With docker-compose
Install [Docker](https://www.docker.com/get-started) and [Docker Compose](https://docs.docker.com/compose/install/).
```shell
docker-compose up
```

## Usage

![](images/example_1.png)
